# BMS

System zarządzania baterią wraz z możliowścią podglądem parametrów bateri oraz możliwoscią dodania podglądu innych parametrów.

# Board
ESP32 Dev Module
boundrate: 115200

# Środowsiko
* ArduinoIDE 2.2.1
* Biblioteka esp32 by Espressif System (`https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json`)
* ArduinoJson by Benoit
* AdruinoWebsockets by Gil
* WebSockets by Markus


Some **content** with _Markdown_ `syntax`.

:::

# Program
```mermaid
graph LR
A[Client]
B(Server)
C(ManSystem)
D(Receiver)
E[AGV]
F[BMS]
G[(DB)]
I(Client)
J[Node-red]
K(NetNode)

B -.-|JSON| A
I -.-|JSON| J

E -.->|RX| D
F -.-|I/O| C
subgraph Program ESP32
G ---K
K --- I
K --- B
D --> G
C --- G
end

```
## NetNode
```mermaid
stateDiagram
	NetworkConnect: Try connect to all networks
	RunAP: Run Access Point
	ConnectPropreties: Update connect properties

	state Net {
		[*] --> NetworkConnect
		NetworkConnect --> ConnectPropreties: connected
		NetworkConnect --> RunAP: not connected

		RunAP -->ConnectPropreties
		ConnectPropreties --> RunServer
	}

```
## Server
```mermaid
	stateDiagram

	RunServer: Run Server (Loop)
	NewData: New Data
	Signal: Update signal strength
	Update: Update server

	state RunServer {
		[*] --> Signal
		Signal --> NewData
		NewData --> [*]: False
		NewData --> Update: True
		Update --> [*]

	}
```

## DataBase
* const struct Network (read-only)
	* SSID
	* PASS
* const struct AccesPoint (read-only)
	* IPv4
	* SSID
	* PASS
* struct Connection
	* int mode(AP=0/ST=1)
	* string IPv4
	* int strength
* struct dataBMS
	* ...
* struct dataAGV
	* ...
* bool updated
