
root = "./Webside/";
index = root + "index.html";
output = "./Firmware/Webside.hpp";


css_file = [];
css_inc_beg = "<link rel='stylesheet' href='";
css_inc_end = "'>";

js_file = [];
js_inc_beg = "<script src='";
js_inc_end = "'></script>";


# load index #
main = open(index, "r" ).read();

# changed " to ' #
main = main.replace("\"","'");

# search CSS #
idx = 0;
while True:
	idx = main.find(css_inc_beg, idx);
	if idx == -1: break;
	beg = idx + len(css_inc_beg);
	idx = main.find(css_inc_end, idx);
	end = idx;
	if(main[beg:end].find('http') != -1): continue;
	css_file.append(main[beg:end]);
	pass

# search JS #
idx = 0;
while True:
	idx = main.find(js_inc_beg, idx);
	if idx == -1: break;
	beg = idx + len(js_inc_beg);
	idx = main.find(js_inc_end, idx);
	end = idx;
	if(main[beg:end].find('http') != -1): continue;
	js_file.append(main[beg:end]);
	pass

# include CSS #
for f in css_file:
	css = open(root+f, "r" ).read();
	css = css.replace("\"","'");
	main = main.replace(
		css_inc_beg + f + css_inc_end,
		"<style>" + css + "</style>"
	);

# include JS #
for f in js_file:
	js = open(root+f, "r" ).read();
	js = js.replace("\"","\\\"");
	main = main.replace(
		js_inc_beg + f + js_inc_end,
		"<script>" + js + "</script>"
	);

# remove \n \t #
# main = main.replace("\n", "\\\n");
main = main.replace("\n", "");
main = main.replace("\t", "");

code = "#ifndef WEBSIDE_HPP\n";
code += "#define WEBSIDE_HPP\n";
code += "\n";
code += "#define HTML_HOMEPAGE \"" + main + "\"\n";
code += "\n";
code += "#endif /* NETNODE_HPP */\n";

f = open(output, "w");
f.write(code);
f.close();
print(main);
