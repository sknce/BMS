var Socket;
const PRECSION = 3;
const ctx_battery = document.getElementById('chart-battery').getContext('2d');
const table_cells = document.getElementById('table-cells').getElementsByTagName('tbody')[0];
const tile_voltage = document.getElementById('tile-voltage');
const tile_charge = document.getElementById('tile-charge');
const tile_predict_charge = document.getElementById('tile-predict-charge');
const footer = document.getElementsByTagName('footer')[0];
var ooo = {
	"BMS": {
		"Version": "0.0.0-Alpha",
		"Battery": {
			"Status": 0,
			"Voltage": {"Unit": "V", "x": [1,2,3], "y":[3.0, 2.8, 2.5]},
			"Charge": {"Unit": "%", "x": [1,2,3], "y":[88, 79, 0]},
			"PredictCharge": 0,
			"CellNumber": 18,
			"Cell" : [
				{"Status": 0, "Charge": 30, "Voltage": 3.2, "PredictCharge": 0},
				{"Status": 0, "Charge": 40, "Voltage": 3.2, "PredictCharge": 1},
				{"Status": 0, "Charge": 23, "Voltage": 3.2, "PredictCharge": 2},
				{"Status": 0, "Charge": 43, "Voltage": 3.2, "PredictCharge": 3},
				{"Status": 0, "Charge": 10, "Voltage": 3.2, "PredictCharge": 4},
				{"Status": 0, "Charge": 34, "Voltage": 3.2, "PredictCharge": 5},
				{"Status": 0, "Charge": 11, "Voltage": 3.2, "PredictCharge": 6},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 7},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 8},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 9},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 10},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 11},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 12},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 13},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 14},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 15},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 16},
				{"Status": 0, "Charge": 100, "Voltage": 3.2, "PredictCharge": 17},
			]
		}
	} /* end BMS */
};
function open_socket() {
	Socket = new WebSocket('ws://' + window.location.hostname + ':81/');
	Socket.onmessage = function(event) {
		socket_reveive(event);
	};
}
function socket_reveive(event) {
	var obj = JSON.parse(event.data);
	ooo = obj;
	console.log("RX:");
	console.log(event.data);
	update_cells(obj);
	update_footer(obj);
	update_battery(obj);


}
function update_footer(obj) {
	footer.innerHTML = obj["BMS"]["Version"];
}
function update_battery(obj) {
	var data = obj["BMS"]["Battery"]["Voltage"];
	update_tile(tile_voltage, "Voltage", data["y"][0].toFixed(PRECSION), data["Unit"]);

	data = obj["BMS"]["Battery"]["PredictCharge"];
	update_tile(tile_predict_charge, "Charge Time Left", seconds_to_hhmm(data), "");
	if(data == 0) {
		tile_predict_charge.style.display = "none";
	}

	data = obj["BMS"]["Battery"]["Charge"];
	update_tile(tile_charge, "Charge", data["y"][0], data["Unit"]);
	chart(ctx_battery, data["x"], data["y"], "Charge [%]");
}
function update_cells(obj) {
	var cell = obj["BMS"]["Battery"]["Cell"];
	for(var i = 0; i < obj["BMS"]["Battery"]["CellNumber"]; i++) {
		add_cell(table_cells, i+1, cell[i]);
	}
}
function update_tile(tile, label, value, unit) {
	tile.getElementsByTagName("h2")[0].innerHTML = label;
	tile.getElementsByTagName("p")[0].innerHTML = value;
	tile.getElementsByTagName("span")[0].innerHTML = unit;
}
function add_cell(parent, id, data) {
	var cell = document.createElement("tr");
	/* Cell */
	cell.innerHTML = "<td>\
					<div class='box-number'>" + id + "</div>\
					</td>";
	/* Charge */
	cell.innerHTML += "<td>\
					<progress value=" + data["Charge"] + " max='100'></progress> \
					<div class='progress-value'>" + data["Charge"] +"%</div>\
					</td>";
	/* Voltage */
	cell.innerHTML += "<td>\
					<div class='box'>"+ data["Voltage"].toFixed(PRECSION) + "V</div>\
					</td>";
	/* Charge Predict Time */
	cell.innerHTML += "<td>\
					<div class='box'>"+ seconds_to_hhmm(data["PredictCharge"]) + "</div>\
					</td>";
	parent.appendChild(cell);
}
function chart(ctx, x, y, label, color = '#3498db') {
	const chart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: x.reverse(),
			datasets: [{
				label: label,
				borderColor: color,
				backgroundColor: 'rgba(52, 152, 219, 0.2)',
				data: y.reverse(),
				tension: 0.0,
			}]
		},
		options: {
			responsive: true,
			maintainAspectRatio: false,
		}
	});

}
function seconds_to_hhmm(sec) {
	var hh = parseInt(sec / 3600);
	var mm = parseInt((sec % 3600) / 60);
	var ss = parseInt(sec % 60);
	var str = "";
	str += hh == 0 ? "" : hh + "h ";
	str += mm == 0 ? "" : mm + "m ";
	str = (ss < 60)&&(str == "") ? ss + "s" : str;
	str = str == "" ? "--:--" : str;
	return str;
}

window.onload = function(event) {
	console.log("Hello World");
	open_socket();
}
