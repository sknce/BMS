const table_networks = document.getElementById('table-networks').getElementsByTagName('tbody')[0];
const input_network_ssid = document.getElementById('input-network-ssid')
const input_network_pass = document.getElementById('input-network-pass')

var obj = {
	"Network": [
		{"SSID": "net1"},
		{"SSID": "net2"},
		{"SSID": "net3"},
		{"SSID": "net4"},
	] /* end Network */
}

function init() {
	console.log("Hello World!");
	console.log(obj);
	update_network()
}
function update_network() {
	var network = obj["Network"];
	for(var i = 0 ; i < network.length; i++) {
		show_network(table_networks, i, network[i]["SSID"]);
	}
}
function remove_network(id, ssid) {
	var net = obj["Network"];

	if(net[id]["SSID"] == ssid && id > -1) {
		net.splice(id, 1);
		const row_id = id + "-" + ssid;
		document.getElementById(row_id).remove();
	}
}
function add_network() {
	var net = obj["Network"];
	const ssid = input_network_ssid.value;
	const pass = input_network_pass.value;

	if(ssid != '') {
		/* PASS sholud be encrypted */
		net.push({"SSID": ssid, "PASS": pass});
		console.log(net);
		/*Send to server*/
	} else {
		alert("SSID is empty!");
	}

}
function show_network(parent, id, ssid) {
	const row_id = id + "-" + ssid;
	const callback = "\'remove_network("+ id +", \""+ ssid +"\")\'";
	var network = document.createElement("tr");
	network.setAttribute("id", row_id);
	/* SSID */
	network.innerHTML = "<td class='box-long'>\
					<div class='box'>" + ssid + "</div>\
					</td>";
	/* Button */
	network.innerHTML += "<td>\
					<input class='button button-red' \
						type='button'\
						onclick=" + callback + " \
						value='Remove'>\
					</td>";
	parent.appendChild(network);
}



window.onload = init();
