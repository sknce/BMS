#ifndef DATABASE_HPP
#define DATABASE_HPP

#include "HardwareSerial.h"
#include "WString.h"
#include <sys/types.h>
#include <sys/_stdint.h>
#include <EEPROM.h>

#include "BmsObject.hpp"

namespace bms {
class DataBase {
public:
  BmsObject* bms = NULL;
public:
  DataBase(BmsObject* bms);
  String get_json();
};
} /* bms */


#endif /* end of include guard: DATABASE_HPP */
