#ifndef DBOBJECT_HPP
#define DBOBJECT_HPP

#include <ArduinoJson.h>

class DBObject {
public:
  virtual void load_from_memory() {};
  virtual void save_to_memory() {};

  virtual void add_to_json(JsonObject* parent) {};
  virtual void update_from_json(JsonObject* parent) {};

};

#endif /* end of include guard: DBOBJECT_HPP */