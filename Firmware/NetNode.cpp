#include "NetNode.hpp"

NetNode::Net::Net(
  const char* ssid,
  const char* pass
) : 
  ssid(ssid),
  pass(pass) {
}

NetNode::NetNode(
  Net AP_NET,
  IPAddress AP_IPv4,
  IPAddress AP_GATEWAY,
  IPAddress AP_SUBNET,
  Net* ST_NET,
  uint32_t ST_NET_LEN,
  IPAddress ST_IPv4,
  IPAddress ST_GATEWAY,
  IPAddress ST_SUBNET,
  IPAddress ST_PRIMARY_DNS,
  IPAddress ST_SECONDARY_DNS
) : 
  AP_NET(AP_NET),
  AP_IPv4(AP_IPv4),
  AP_GATEWAY(AP_GATEWAY),
  AP_SUBNET(AP_SUBNET),
  ST_NET(ST_NET),
  ST_NET_LEN(ST_NET_LEN),
  ST_IPv4(ST_IPv4),
  ST_GATEWAY(ST_GATEWAY),
  ST_SUBNET(ST_SUBNET),
  ST_PRIMARY_DNS(ST_PRIMARY_DNS),
  ST_SECONDARY_DNS(ST_SECONDARY_DNS) {
}
NetNode::Connection NetNode::make_AP() {
  if( strcmp(con.mode, "AP") == 0) {
    return con;
  }
  Connection AP_connection;
  	/* Config AP */
	WiFi.softAPConfig(AP_IPv4, AP_GATEWAY, AP_SUBNET);
	WiFi.softAP(AP_NET.ssid, AP_NET.pass);
	/* Update Connect As AP */
	strcpy(AP_connection.mode, "AP");
	strcpy(AP_connection.ssid, AP_NET.ssid);
	strcpy(AP_connection.IPv4, WiFi.softAPIP().toString().c_str());
	AP_connection.rssi = 0;
  return AP_connection;
}
bool NetNode::connect_to(const char* ssid, const char* pass) {
  WiFi.begin(ssid, pass);
  for (uint32_t count = 0; count < 20; count++) {
	if (WiFi.status() == WL_CONNECTED) {
	  return true;
	}
	delay(200);
  }
  return false;
}
NetNode::Connection NetNode::init() {

  bool connected = false;

  /* Try connect to Network */
  uint8_t i = 0;
  while(connected == false ) {
    
    if(ST_IPv4 && ST_GATEWAY && ST_SUBNET && ST_PRIMARY_DNS && ST_SECONDARY_DNS) {
	    WiFi.config(ST_IPv4, ST_GATEWAY, ST_SUBNET, ST_PRIMARY_DNS, ST_SECONDARY_DNS);
    }
    connected = connect_to(ST_NET[i].ssid, ST_NET[i].pass);
    i++;
  }


  /* Create AP */
  if (connected == false) {
    con = make_AP();
  } else {
	strcpy(con.mode, "ST");
	strcpy(con.ssid, WiFi.SSID().c_str());
	strcpy(con.IPv4, WiFi.localIP().toString().c_str());
	con.rssi = WiFi.RSSI();
  }

  /* Connection params */
  if(Serial) {
	  Serial.printf("mode: %s\n", con.mode);
	  Serial.printf("ssid: %s\n", con.ssid);
	  Serial.printf("IPv4: %s\n", con.IPv4);
	  Serial.printf("RSSI: %d dB\n", con.rssi);
  }

  return con;
}
