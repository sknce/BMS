#ifndef BMSSYSTEM_HPP
#define BMSSYSTEM_HPP

#include "DataBase.hpp"

namespace bms {

class BmsSystem {
private:
  BmsObject* bms;
public:
  BmsSystem(DataBase* db);
};

} /* bms */

#endif /* BMSSYSTEM_HPP */
