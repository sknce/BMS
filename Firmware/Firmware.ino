#define DEBUG
#include <WebServer.h>
#include <WebSocketsServer.h>
#include <ArduinoJson.h>


#include "NetNode.hpp"
#include "Webside.hpp"
#include "BmsObject.hpp"
#include "DataBase.hpp"
#include "BmsSystem.hpp"



WebSocketsServer socket(81);
WebServer server(80);

NetNode::Net AP_NET("BMS", "");
NetNode::Net ST_NETS[] = {
  NetNode::Net("xd", "w"),
  NetNode::Net("cztery", "4"),
  NetNode::Net("bezpolskichznakow", "74"),
  NetNode::Net("czrodziejeImagnaci", "masz2kijInapierdalaj")
};
NetNode net (
  AP_NET,
  IPAddress(192, 168, 4, 22),
  IPAddress(192, 168, 4, 9),
  IPAddress(255, 255, 255, 0),
  ST_NETS,
  4,
  IPAddress(192, 168, 0, 202),
  IPAddress(192, 168, 0, 1),
  IPAddress(255, 255, 0, 0)
);

bms::BmsObject* bms_data = NULL;
bms::DataBase* database = NULL;
// bms::BmsSystem system_bms(database);

String hp = HTML_HOMEPAGE;

void setup() {

  Serial.begin(115200);
  Serial.println("Serial start:");
  /* DataBase */
  bms_data = new bms::BmsObject(18);

  database = new bms::DataBase(bms_data);

  /* Network */
  net.init();
  net.make_AP();
  server.on(
    "/",
    []() {
      server.send(200, "text\html", hp);
    });
  server.begin();
  socket.begin();
  socket.onEvent(webSocketsEvent);
}
uint64_t ptime = millis();
void loop() {
  /* Network */
  server.handleClient();
  socket.loop();
  // if ((millis() - ptime) > 10000) {
  //   String str_json = database->get_JSON();
  //   socket.broadcastTXT(str_json);
  //   ptime = millis();

  //   Serial.printf("\ntime: %d\n", ptime);

  // }
}

void webSocketsEvent(byte num, WStype_t type, uint8_t* payload, size_t len) {
  switch (type) {
    case WStype_DISCONNECTED:
      {
        // TODO:

        Serial.println("Client disconected");

        break;
      }
    case WStype_CONNECTED:
      {
        // TODO:
        String str_json = database->get_json();
        socket.broadcastTXT(str_json);

        Serial.println("Client connected");

        break;
      }
    case WStype_TEXT:
      {
        // TODO:
        // database.set_JSON(payload, len);

        Serial.println("Get text");

        break;
      }
  }
}
