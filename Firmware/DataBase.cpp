#include "DataBase.hpp"


/* +-------------------- DataBase --------------------+ */
bms::DataBase::DataBase(BmsObject* bms) : bms(bms) {

}

String bms::DataBase::get_json() {
  String str_json = "";
  StaticJsonDocument<3048> doc;
  JsonObject obj = doc.to<JsonObject>();

  bms->add_to_json(&obj);

  serializeJson(doc, str_json);
  return str_json;
}
