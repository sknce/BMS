#include <sys/_stdint.h>
#ifndef BMSOBJECT_HPP
#define BMSOBJECT_HPP

#include "DBObject.hpp"
#include "Data.hpp"

namespace bms {
/* +------------- BMS -------------+ */
struct BmsObject : public DBObject {
public:
  struct Cell {
    uint8_t status = 0;
    float voltage = 3.2;
    float charge = 50;
    uint32_t predict_charge = 2000;
  };
  struct Battery {
    uint8_t status = 0;
    Data<float> voltage;
    Data<uint8_t> charge;
    Data<uint32_t> timestamp;
    uint32_t predict_charge = 5700;
    const uint8_t cell_number;
    Cell* cell = NULL;
    Battery(const uint8_t cell_number);
    ~Battery();
  };

public:
  const char* version = "0.0.0-Alpha";
  Battery bat;
  BmsObject(const uint8_t cell_number);
  void add_to_json(JsonObject* parent);
};

} /* bms */

#endif /* end of include guard: BMSOBJECT_HPP */