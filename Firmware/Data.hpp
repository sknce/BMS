#ifndef DATA_HPP
#define DATA_HPP

#include <stdexcept>
#include <sys/types.h>
#include <sys/_stdint.h>


template<class T>
struct Data {
private:
public:
  uint32_t idx = 0;
  uint32_t add = 0;
public:
  T* x = NULL;
public:
  const char* unit;
  const uint32_t size;
  Data(const char* unit, uint32_t size) : unit(unit), size(size > 0 ? size : 1) {
    x = new T[this->size];
    for(uint32_t i = 0; i < this->size; i++) {
      x[i] = (T)0;
    }
  }
  ~Data() {
    delete x;
  }
  void append(T x) {
    this->x[add] = x;
    if(add > 0) {
      add--;
      idx = add + 1;
    } else {
      add = size - 1;
      idx = 0;
    }
  }
  uint32_t tab(T* pdata) {
    for(uint32_t i = 0; i < size; i++) {
      pdata[i] = (*this)[i];
    }
    return size;
  }
  constexpr T& operator[](unsigned int i) {
    if( i >= size ) {
      throw std::out_of_range("index is out of range");
    }
    return x[(idx + i) % size];
  }
};


#endif /* end of include guard: DATA_HPP */