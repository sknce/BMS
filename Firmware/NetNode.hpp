#ifndef NETNODE_HPP
#define NETNODE_HPP

#include <sys/_stdint.h>
#include <WiFi.h>


class NetNode {
public:
  struct Net {
    const char* ssid;
    const char* pass;
    Net(const char* ssid, const char* pass);
  };
  struct Connection {
    char ssid[64] = "";
    char IPv4[64] = "";
    char mode[3] = "";
    int rssi = 0;
  };
private:
  Connection con;
  /* AccesPoint */
  const Net AP_NET;
  IPAddress AP_IPv4 = IPAddress(192, 168, 4, 22);
  IPAddress AP_GATEWAY = IPAddress(192, 168, 4, 9);
  IPAddress AP_SUBNET = IPAddress(255, 255, 255, 0);
  /* Station */
  Net* ST_NET;
  const uint32_t ST_NET_LEN;
  IPAddress ST_IPv4 = IPAddress(192, 168, 0, 202);
  IPAddress ST_GATEWAY = IPAddress(192, 168, 0, 1);
  IPAddress ST_SUBNET = IPAddress(255, 255, 0, 0);
  IPAddress ST_PRIMARY_DNS = IPAddress(8, 8, 8, 8);   //optional
  IPAddress ST_SECONDARY_DNS = IPAddress(8, 8, 4, 4); //optional

public:
  NetNode(
      Net AP_NET,
      IPAddress AP_IPv4,
      IPAddress AP_GATEWAY,
      IPAddress AP_SUBNET,
      Net* ST_NET,
      uint32_t ST_NET_LEN,
      IPAddress ST_IPv4,
      IPAddress ST_GATEWAY,
      IPAddress ST_SUBNET,
      IPAddress ST_PRIMARY_DNS = IPAddress(8, 8, 8, 8),  //optional
      IPAddress ST_SECONDARY_DNS = IPAddress(8, 8, 4, 4) //optional
  );
  inline bool connect_to(const char* ssid, const char* pass);
  Connection make_AP();
  Connection init();
};

#endif /* NETNODE_HPP */
