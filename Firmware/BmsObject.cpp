#include <sys/_stdint.h>
#include "ArduinoJson/Array/Utilities.hpp"
#include "BmsObject.hpp"

/* +-------------------- BMS --------------------+ */
bms::BmsObject::BmsObject(const uint8_t cell_number) : bat(cell_number) {
}
bms::BmsObject::Battery::Battery(
  const uint8_t cell_number
) :
  voltage("V", 10),
  charge("%", 10),
  timestamp("s", 10),
  cell_number(cell_number) {
  cell = new Cell[cell_number];
  Serial.printf("XD Start\n");
  for(int i = 0; i < 15; i++) {
    charge.append(i*2);
    voltage.append(3*i);
    timestamp.append(i);
  }
  // Serial.printf("tab.x = [ ");
  // for(int i = 0; i < 10; i++) {
  //   Serial.printf("%u, ", timestamp.x[i]);
  // }
  // Serial.printf("]\n");
  // Serial.printf("idx = %u\n", timestamp.idx);
  // Serial.printf("tab   = [ ");
  // for(int i = 0; i < 10; i++) {
  //   Serial.printf("%u, ", timestamp[i]);
  // }
  // Serial.printf("]\n");
  

  // unsigned int tab[10];
  // timestamp.tab(tab);
  // Serial.printf("cpy   = [ ");
  // for(int i = 0; i < 10; i++) {
  //   Serial.printf("%u, ", tab[i]);
  // }
  // Serial.printf("]\n");
}
bms::BmsObject::Battery::~Battery() {
  delete cell;
}
void bms::BmsObject::add_to_json(JsonObject* parent) {
  // float _voltage[bat.voltage.size];
  // bat.voltage.tab(_voltage);

  // uint8_t _charge[bat.charge.size];
  // bat.charge.tab(_charge);

  // uint32_t _timestamp[bat.timestamp.size];
  // bat.timestamp.tab(_timestamp);

  /**/
  // Serial.printf("Here: %d/n", bat.timestamp.x[0]);
  (*parent)["BMS"]["Version"]                     = version;
  (*parent)["BMS"]["Battery"]["Status"]           = bat.status;
  (*parent)["BMS"]["Battery"]["PredictCharge"]    = bat.predict_charge;
  (*parent)["BMS"]["Battery"]["CellNumber"]       = bat.cell_number;
  /**/
  (*parent)["BMS"]["Battery"]["Voltage"]["Unit"]  = bat.voltage.unit;
  for(uint32_t i = 0; i < bat.voltage.size; i++) {
    (*parent)["BMS"]["Battery"]["Voltage"]["y"][i] = bat.voltage[i];
  }
  for(uint32_t i = 0; i < bat.timestamp.size; i++) {
    (*parent)["BMS"]["Battery"]["Voltage"]["x"][i] = bat.timestamp[i];
  }
  /**/
  (*parent)["BMS"]["Battery"]["Charge"]["Unit"]   = bat.charge.unit;
  for(uint32_t i = 0; i < bat.charge.size; i++) {
    (*parent)["BMS"]["Battery"]["Charge"]["y"][i] = bat.charge[i];
  }
  for(uint32_t i = 0; i < bat.timestamp.size; i++) {
    (*parent)["BMS"]["Battery"]["Charge"]["x"][i] = bat.timestamp[i];
  }
  /**/
  JsonArray cell = (*parent)["BMS"]["Battery"]["Cell"].to<JsonArray>();
  for(uint8_t i = 0; i < bat.cell_number; i++) {
    StaticJsonDocument<256> c;
    c["Status"]           = bat.cell[i].status;
    c["Charge"]           = bat.cell[i].charge;
    c["Voltage"]          = bat.cell[i].voltage;
    c["PredictCharge"]    = bat.cell[i].predict_charge;
    cell.add(c);
  }
}
